package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {

    void createPost(String stringToken, Post post);

    //We can iterate/loop over the posts in order to display them
    Iterable<Post> getPosts();

    ResponseEntity deletePost(Long id, String stringToken);

    ResponseEntity updatePost(Long id, String stringToken, Post post);

    Iterable<Post> getMyPosts(String stringToken);
}
